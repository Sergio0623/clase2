package com.kaizen.others.curso.database.mysql;

import java.util.LinkedList;

public class Clase4
{
    public void addContadorAntiguio(int numero)
    {
        LinkedList<Clase3> lista = Clase3.getLista();
        // for ( variable = 0; condicion; incremente   )

        int contador = lista.size();
        for (int i=0; i<contador; i++)
        {
            Clase3 mivar = lista.get(i);
            mivar.addNumber(numero);
        }
    }

    public void addContadorNuevo(int numero)
    {
        LinkedList<Clase3> lista = Clase3.getLista();

        for (Clase3 mivar : lista)
        {
            mivar.addNumber(numero);
        }
    }
}
