package com.kaizen.others.curso.comunicaciones;

public class ClaseA
{
    ClaseB mivar;

    public ClaseA()
    {
        mivar = new ClaseB();
    }

    public void escribirNombre(String nombre, String apellido)
    {
        System.out.println("El nombre es: " + nombre);
        mivar.apellido(apellido);
    }
}
