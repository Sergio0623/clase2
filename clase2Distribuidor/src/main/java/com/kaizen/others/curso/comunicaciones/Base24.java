package com.kaizen.others.curso.comunicaciones;

import com.ks.lib.tcp.Cliente;
import com.ks.lib.tcp.EventosTCP;
import com.ks.lib.tcp.Servidor;
import com.ks.lib.tcp.Tcp;

public class Base24 extends Servidor implements EventosTCP
{
    public Base24()
    {
        setEventos(this);
    }

    @Override
    public void conexionEstablecida(Cliente cliente)
    {

    }

    @Override
    public void errorConexion(String s)
    {

    }

    @Override
    public void datosRecibidos(String s, byte[] bytes, Tcp tcp)
    {
        Distribuidor.sendMessage(s);
    }

    @Override
    public void cerrarConexion(Cliente cliente)
    {

    }
}
