package com.kaizen.others.curso;

import com.kaizen.others.curso.comunicaciones.Base24;
import com.kaizen.others.curso.comunicaciones.Distribuidor;
import com.sun.org.apache.xalan.internal.xsltc.dom.CachedNodeListIterator;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // 1.- Crear conexiones clientes

        try
        {
            Distribuidor[] cliente = new Distribuidor[2];
            cliente[0] = new Distribuidor();
            cliente[0].setIP(System.getProperty("dis1.ip"));
            cliente[0].setPuerto(Integer.parseInt(System.getProperty("dis1.port")));
            cliente[0].conectar();

            cliente[1] = new Distribuidor();
            cliente[1].setIP(System.getProperty("dis2.ip"));
            cliente[1].setPuerto(Integer.parseInt(System.getProperty("dis2.port")));
            cliente[1].conectar();
        }
        catch (Exception ex)
        {
            System.out.println("Problema al crear los clientes de comunicaciones " + ex);
        }

        // 2. crear el servidor donde recibo transacciones
        try
        {
            Base24 servidor = new Base24();
            servidor.setPuerto(Integer.parseInt(System.getProperty("base24.puerto")));
            servidor.conectar();
        }
        catch (Exception ex)
        {
            System.out.println("Problema al crear el servidor de comunicaciones " + ex);
        }
    }
}
